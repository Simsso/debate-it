var http = require('http');
var uuid = require('uuid');
var fs = require('fs');
var server = http.createServer();
var io = require('socket.io')(server);

var clients = [];
//var questions = ['gay marriage','the minimum wage','privatisation of the NHS','categorical imperatives','Scottish independence','immigration','freedom of the press','secret trials','free will','nuclear weapons','extra-terrestrial life','the human rights act','the human soul','the meaning of life','wind farms','contraception','firearms','European union']; 
var questions = ['the minimum wage','privatisation of the NHS','immigration','freedom of the press','European union']; 

io.on('connection', function (socket) {
    socket.id = uuid.v4();
    clients.push({
        socket: socket,
        formData: [],
        partner: undefined,
        room: undefined
    });
    console.log('user connected    ' + getDateTime() + ' (' + (clients.length - 1) + ' - ' + socket.id + ')' + ' [' + clients.length + '/' + countDebates() + ']');

    socket.on('debateMessage', function (data) {
        if (data.message && clients[getClientIndexById(socket.id)].partner)
            clients[getClientIndexById(socket.id)].partner.socket.emit('debateMessage', data);
    });
    
    // add listener to receive form data
    socket.on('formData', function (answers) {
        // add name listener
        socket.on('userName', function (name) {
            if (name && name.length < 30) {
                name = escapeHtml(name); 
                clients[getClientIndexById(socket.id)].partner.socket.emit('partnerName', name); 
            }
        }); 
        
        // save form data in a file
        
        var answersLocal = answers.slice(0); 
        fs.readFile('data.json', 'utf8', function (err, data) {
            if (err != null) 
                return; 
            try {
            var jsonData = JSON.parse(data); 
            }
            catch (e) {
                return; 
            }
            var date = new Date; 
            for (var i = 0; i < answersLocal.length; i++) {
                if (answersLocal[i] == 'disagree') 
                    answersLocal[i] = -1;
                else if (answersLocal[i] == 'agree')
                    answersLocal[i] = 1;
                else 
                    answersLocal[i] = 0; 
            }
            answersLocal.unshift(date.getTime());
            jsonData.push(answersLocal); 
            fs.writeFile('data.json', JSON.stringify(jsonData), 'utf8');
           // console.log('inside callback' + answers); 
            
        });
        //console.log(answers); 
        clients[getClientIndexById(socket.id)].formData = answers; 
        
        // search for partner
        var debate = findePeopleToDebate();
        if (debate.found != -1) { // partner found
            var roomId = uuid.v4(); 
            console.log('debate started    ' + getDateTime() + ' (' + (countDebates()) + ' - ' + roomId + ')' + ' [' + clients.length + '/' + (countDebates() + 1) + ']');
            
            // refresh clients array data
            partnerA = clients[debate.searching]; 
            partnerB = clients[debate.found];
            partnerA.partner = partnerB;
            partnerB.partner = partnerA;
            
            // add message listener for both partners
            partnerA.socket.join(roomId);
            partnerB.socket.join(roomId); 
            
            // send discussion topic
            var debateTopic = '"' + questions[debate.questionIndex] + '"';
            partnerA.socket.emit('discussionTopic', debateTopic); 
            partnerB.socket.emit('discussionTopic', debateTopic); 
            
        }
    });
    

    socket.on('disconnect', function() {
        if (clients[getClientIndexById(socket.id)].partner) { // check if user was in conversation
            clients[getClientIndexById(socket.id)].partner.socket.emit('partnerDisconnected'); // send notification to partner
        }
        console.log('user disconnected ' + getDateTime() + ' (' + (clients.length - 1) + ' - ' + socket.id + ')' + ' [' + clients.length + '/' + countDebates() + ']'); 
        clients.splice(getClientIndexById(socket.id), 1); // delete user
    });
});

server.listen(3000, function() {
    console.log('node server started'); 
    console.log('listening on *:3000');
});

function getClientIndexById (id) {
    for (var i = 0; i < clients.length; i++) 
        if (clients[i].socket.id == id) 
            return i; 
}

function countDebates() {
    var count = 0; 
    for (var i = 0; i < clients.length; i++) 
        if (clients[i].partner)
            count += .5;
    return count; 
}


function getDateTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? '0' : '') + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? '0' : '') + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? '0' : '') + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? '0' : '') + month;
    var day  = date.getDate();
    day = (day < 10 ? '0' : '') + day;
    return year + '.' + month + '.' + day + ' - ' + hour + ':' + min + ':' + sec;
}

function findePeopleToDebate () {
    var searchingIndex = clients.length - 1;
    for (var i = 0; i < clients.length - 1; i++) {
        if (!clients[i].partner) {
            var differences = 0, jGlobal = 0; 
            for (j = 0; j < clients[searchingIndex].formData.length; j++) {
                if (clients[i].formData[j] != clients[searchingIndex].formData[j] && 
                    (clients[i].formData[j] != 'neutral' && clients[searchingIndex].formData[j] != 'neutral')) {
                    differences++;
                    jGlobal = j; 
                }
            }
            if (differences > 0 && differences < 4) // number of questions that can be unequal
                return { searching: searchingIndex, found: i,  questionIndex: jGlobal}; 
        }
    }
    return { searching: searchingIndex, found: -1}
}

function escapeHtml(text) {
              return text
                  .replace(/&/g, "&amp;")
                  .replace(/</g, "&lt;")
                  .replace(/>/g, "&gt;")
                  .replace(/"/g, "&quot;")
                  .replace(/'/g, "&#039;");
            }